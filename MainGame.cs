using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Pathfinding.Util;
using Pathfinding.UI;
using Pathfinding.World.Map;
using Pathfinding.Components;
using Pathfinding.Components.Input;
using Pathfinding.World.Objects;
using Pathfinding.Algorithms;



namespace Pathfinding
{

    public class MainGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        CameraComponent camera;
        SelectionBoxComponent selectionBox;
        MouseHandler mouse;
        KeyboardHandler keyboard;
        TileMap map;
        Unit unit;
        Unit selectedUnit = null;
        bool isBuild;

        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1270;
            graphics.PreferredBackBufferHeight = 720;
            IsMouseVisible = true;
            camera = new CameraComponent(this);
            mouse = new MouseHandler(this);
            keyboard = new KeyboardHandler(this);
            selectionBox = new SelectionBoxComponent(this);
            unit = new Unit(this, new Vector2(67, 67), camera);
            mouse.LeftClick +=new EventHandler<MouseEventArgs>(mouse_LeftClick);
            mouse.RightClick += new EventHandler<MouseEventArgs>(mouse_RightClick);
            Components.Add(mouse);
            Components.Add(keyboard);
            Components.Add(camera);
            Components.Add(selectionBox);
            Components.Add(unit);
            Components.Add(selectedUnit);
            map = new TileMap();
            AStar.Map = map;
            Content.RootDirectory = "Content";
            isBuild = false;
        }

        void mouse_RightClick(object sender, MouseEventArgs e)
        {
            if(selectedUnit != null)
            {
                selectedUnit.Goal = (sender as MouseHandler).Position;
                selectedUnit.HasGoal = true;
            }
        }

        void  mouse_LeftClick(object sender, MouseEventArgs e)
        {
            if (!selectionBox.IsRisizing && isBuild)
            {
                Vector2 mcoor = (sender as MouseHandler).TransformPosition(camera);
                Tile t = map.AtCoordinates(mcoor.X, mcoor.Y);
                if (t != null)
                {
                    t.Toggle();
                }
            }
            else
            {
                if (unit.GetPosition().Contains(mouse.TransformPositionPoint(camera)))
                {
                    unit.Selected = true;
                    unit.Preselected = false;
                    selectedUnit = unit;
                }
                else
                {
                    unit.Preselected = false;
                    unit.Selected = false;
                    selectedUnit = null;
                }
            }
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Texture2D pixel = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            pixel.SetData(new[] { Color.White });
            TextureLibrary.AddTexture("pixel", pixel);
            Tile.Font = Content.Load<SpriteFont>("font");
        }

        protected override void UnloadContent()
        {
            TextureLibrary.Clear();
        }

        protected override void Update(GameTime gameTime)
        {
            if (!isBuild)
            {
                if (!mouse.Pressed && !mouse.LeftClicked)
                {
                    if (unit.GetPosition().Contains(mouse.TransformPositionPoint(camera)))
                    {
                        unit.Preselected = true;
                    }
                    else
                    {
                        unit.Preselected = false;
                    }
                }
                if (mouse.Pressed && !selectionBox.IsRisizing)
                {
                    selectionBox.Start(mouse.Position);
                }
                if (mouse.Drag && selectionBox.IsRisizing)
                {
                    selectionBox.Resize(mouse.Position);
                }
                if (mouse.Released && selectionBox.IsRisizing)
                {
                    selectionBox.End();
                }
            }
            if(keyboard.Pushed(Keys.B))
            {
                isBuild = !isBuild;
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin(SpriteSortMode.BackToFront,BlendState.AlphaBlend, SamplerState.PointClamp,null,null,null,camera.Transform);
            map.Draw(spriteBatch, camera);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
