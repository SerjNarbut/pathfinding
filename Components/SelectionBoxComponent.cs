using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Pathfinding.World.Objects;
using Pathfinding.Primitives;
using Pathfinding.Util;


namespace Pathfinding.Components
{
    public class SelectionBoxComponent : Microsoft.Xna.Framework.DrawableGameComponent
    {
        protected IEnumerable<IUnit> seletedUnits = null;
        protected Rectangle areaRectangle;
        protected SpriteBatch spriteBatch;

        public bool IsRisizing { get; set; }

        public SelectionBoxComponent(Game game): base(game)
        {
        }

        public override void Initialize()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            seletedUnits = UnitHelper.FilterSelectableUnits(Game.Components);
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            
            if (areaRectangle.Width != 0 || areaRectangle.Height != 0)
            {
                Line lOne = new Line(new Vector2(areaRectangle.X, areaRectangle.Y), new Vector2(areaRectangle.X + areaRectangle.Width, areaRectangle.Y), 2,Color.Green);
                Line ltwo = new Line(new Vector2(areaRectangle.X, areaRectangle.Y + areaRectangle.Height), new Vector2(areaRectangle.X + areaRectangle.Width, areaRectangle.Y + areaRectangle.Height), 2, Color.Green);
                Line lthree = new Line(new Vector2(areaRectangle.X, areaRectangle.Y), new Vector2(areaRectangle.X, areaRectangle.Y + areaRectangle.Height), 2, Color.Green);
                Line lfour = new Line(new Vector2(areaRectangle.X + areaRectangle.Width, areaRectangle.Y), new Vector2(areaRectangle.X + areaRectangle.Width, areaRectangle.Y + areaRectangle.Height), 2, Color.Green);

                spriteBatch.Begin();
                lOne.Draw(spriteBatch);
                ltwo.Draw(spriteBatch);
                lthree.Draw(spriteBatch);
                lfour.Draw(spriteBatch);
                spriteBatch.End();
            }
            base.Draw(gameTime);
        }

        public void Start(Vector2 position)
        {
            areaRectangle = new Rectangle((int)position.X, (int)position.Y, 0, 0);
            IsRisizing = true;
        }

        public IEnumerable<IUnit> Resize(Vector2 position)
        {
            areaRectangle = new Rectangle(areaRectangle.X, areaRectangle.Y, (int)position.X - areaRectangle.X, (int)position.Y - areaRectangle.Y);
            return seletedUnits;
        }

        public IEnumerable<IUnit> End()
        {
            reset();
            return seletedUnits;
        }

        private void reset()
        {
            IsRisizing = false;
            areaRectangle = new Rectangle(-1,-1,0,0);
        }
    }
}
