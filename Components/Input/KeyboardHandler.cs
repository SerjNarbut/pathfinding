using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Pathfinding.Components.Input
{
    public class KeyboardHandler : Microsoft.Xna.Framework.GameComponent
    {
        protected KeyboardState currentState;
        protected KeyboardState oldState;
        public KeyboardHandler(Game game): base(game)
        {
        }

        public override void Initialize()
        {
            currentState = oldState = Keyboard.GetState();
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            oldState = currentState;
            currentState = Keyboard.GetState();
            base.Update(gameTime);
        }

        public bool Pressed(Keys key)
        {
            return oldState.IsKeyDown(key) && currentState.IsKeyDown(key);
        }

        public bool Pushed(Keys key)
        {
            return oldState.IsKeyUp(key) && currentState.IsKeyDown(key);
        }
    }
}
