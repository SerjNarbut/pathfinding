using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Pathfinding.UI;


namespace Pathfinding.Components.Input
{
    public class MouseEventArgs : EventArgs
    {
        public Vector2 position;
        public MouseEventArgs(Vector2 pos)
        {
            this.position = pos;
        }
    }


    public class MouseHandler : Microsoft.Xna.Framework.GameComponent
    {

        public  event EventHandler<MouseEventArgs> RightClick;
        public  event EventHandler<MouseEventArgs> LeftClick;

        protected MouseState currentState;
        protected MouseState prevState;
        protected int lastScroll;

        protected Vector2 currentPosition;
        protected Vector2 prevPosition;

        public MouseHandler(Game game): base(game)
        {
            
        }

        public override void Initialize()
        {
            currentState = prevState = Mouse.GetState();
            currentPosition = prevPosition = Vector2.Zero;
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            prevState = currentState;
            prevPosition = currentPosition;
    
            currentState = Mouse.GetState();
            lastScroll = currentState.ScrollWheelValue;
            currentPosition.X = currentState.X;
            currentPosition.Y = currentState.Y;

            if (LeftClicked)
            {
                if (LeftClick != null)
                {
                    LeftClick(this, new MouseEventArgs(Position));
                }
            }

            if (RightClicked)
            {
                if (RightClick != null)
                {
                    RightClick(this, new MouseEventArgs(Position));
                }
            }

            base.Update(gameTime);
        }

        public bool LeftClicked
        {
            get
            {
                return prevState.LeftButton == ButtonState.Released && currentState.LeftButton == ButtonState.Pressed;
            }
        }

        public bool RightClicked
        {
            get
            {
                return prevState.RightButton == ButtonState.Released && currentState.RightButton == ButtonState.Pressed;
            }
        }

        public bool Pressed
        {
            get
            {
                return currentState.LeftButton == ButtonState.Pressed;
            }
        }

        public bool Released
        {
            get
            {
                return prevState.LeftButton == ButtonState.Pressed && currentState.LeftButton == ButtonState.Released;
            }
        }

        public bool Moved
        {
            get
            {
                return prevPosition != currentPosition;
            }
        }

        public bool Drag
        {
            get { return Pressed && Moved; }
        }

        public bool Scrolled
        {
            get
            {
                return lastScroll != currentState.ScrollWheelValue;
            }
        }

        public bool ScrolledMax
        {
            get
            {
                return lastScroll < currentState.ScrollWheelValue;
            }
        }

        public bool ScrolledMin
        {
            get
            {
                return lastScroll > currentState.ScrollWheelValue;
            }
        }

        public int ScrollValue
        {
            get
            {
                return currentState.ScrollWheelValue;
            }
        }

        public Vector2 Position
        {
            get
            {
                return currentPosition;
            }
        }

        public Point TransformPositionPoint(CameraComponent camera)
        {
            Vector2 tmp = TransformPosition(camera);
            return new Point((int)tmp.X, (int)tmp.Y);
        }

        public MouseState State
        {
            get
            {
                return currentState;
            }
        }

        public Vector2 TransformPosition(CameraComponent camera)
        {
            return Vector2.Transform(Position, camera.InverseTransform);
        }
    }
}
