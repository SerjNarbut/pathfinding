﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Pathfinding.Util;


namespace Pathfinding.Primitives
{
    class Line
    {
        protected Vector2 start;
        protected Vector2 end;
        protected float width;
        protected Color color;


        public Line(Vector2 start, Vector2 end, float width, Color color)
        {
            this.start = start;
            this.end = end;
            this.width = width;
            this.color = color;
        }

        public Vector2 Start
        {
            get { return start; }
            private set { start =  value; }
        }

        public Vector2 End
        {
            get { return end; }
            private set { end = value; }
        }

        public float Width
        {
            get { return width; }
            set { width = value; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            float angle = (float)Math.Atan2(End.Y - Start.Y, End.X - Start.X);
            float legth = Vector2.Distance(Start, End);
            spriteBatch.Draw(TextureLibrary.GetTexture("pixel"), Start, null, color,
                angle, Vector2.Zero, new Vector2(legth, width), SpriteEffects.None, 0);
        }
    }
}
