﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Pathfinding.Util;

namespace Pathfinding.World.Map
{
    class Tile
    {
        public static readonly int Size = 32;
        private object locked = new object();
        public static SpriteFont Font;
        protected Vector2 directionVector;
        public Point ScreenCoord { get; private set; }
        public Point WorldCoor { get; private set; }
        public TileState State { get; set; }


        public Vector2 DV
        {
            get
            {
                lock (locked)
                {
                    return directionVector;
                }
            }

            set
            {
                lock (locked)
                {
                    directionVector = value;
                }
            }
        }

        public Tile(Point position,Point screenPosition)
        {
            ScreenCoord = screenPosition;
            WorldCoor = position;
            State = TileState.Walkable;
            DV = new Vector2(0, -1);
        }

        public bool IsWalkable
        {
            get
            {
                return State == TileState.Walkable;
            }
        }

        public void Toggle()
        {
            lock (locked)
            {
                if (State == TileState.Walkable)
                    State = TileState.Unwalkable;
                else if (State == TileState.Unwalkable)
                    State = TileState.Walkable;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.DrawString(Tile.Font, string.Format("{0},{1}", WorldCoor.X, WorldCoor.Y), new Vector2(ScreenCoord.X, ScreenCoord.Y), Color.Red);
            if (!IsWalkable)
            {
                spriteBatch.Draw(TextureLibrary.GetTexture("pixel"), new Rectangle(ScreenCoord.X,ScreenCoord.Y,Size,Size), Color.Black);
            }

        }
    }

    enum TileState
    {
        Walkable,
        Unwalkable,
        AIUse
    }
}
