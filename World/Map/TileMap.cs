﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Pathfinding.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Pathfinding.UI;
using Pathfinding.Primitives;

namespace Pathfinding.World.Map
{
    class TileMap
    {
        public readonly int SIZE = 256;
        private Tile[,] tiles = null;
        private object loc = new object();


        public TileMap()
        {
            this.tiles = new Tile[SIZE, SIZE];
            initTiles();
        }


        private void initTiles()
        {
            if (tiles != null)
            {
                for (int i = 0; i < SIZE; i++)
                {
                    for (int j = 0; j < SIZE; j++)
                    {
                        tiles[i, j] = new Tile(new Point(i, j), new Point(i * Tile.Size, j * Tile.Size));
                    }
                }
            }
            else
            {
                throw new NullReferenceException("it not allocate memory for tiles");
            }
        }

        public void Draw(SpriteBatch spriteBatch, CameraComponent camera)
        {
            foreach (Tile t in tiles)
            {
                t.Draw(spriteBatch);
            }
            drawLines(spriteBatch,camera);
        }

        private void drawLines(SpriteBatch spriteBatch,CameraComponent camera)
        {
            foreach (Tile t in tiles)
            {
                t.Draw(spriteBatch);
            }
            for (int i = 0; i <= SIZE; i++)
            {
                Line l = new Line(new Vector2(1, i * Tile.Size),new Vector2(SIZE * Tile.Size, i * Tile.Size), 2,Color.Black);
                l.Draw(spriteBatch);
            }
            for (int i = 0; i <= SIZE; i++)
            {
                Line l = new Line(new Vector2(i * Tile.Size, 1), new Vector2(i * Tile.Size, SIZE * Tile.Size), 2, Color.Black);
                l.Draw(spriteBatch);
            }
        }

        public Tile AtPosition(int x, int y)
        {
            if(x >= 0 && x < SIZE && y >=0 && y <SIZE)
                return tiles[x, y];
            return null;
        }

        public Tile AtCoordinates(float x, float y)
        {
            Point p = coordToPos(x, y);
            return AtPosition(p.X, p.Y);
        }

        private Point coordToPos(float x, float y)
        {
            int xpos = (int)Math.Ceiling((double)(SIZE * x / (SIZE*Tile.Size))) - 1;
            int ypos = (int)Math.Ceiling((double)(SIZE * y / (SIZE * Tile.Size))) - 1;
            return new Point(xpos,ypos);
        }

        public IEnumerable<Tile> GetNeighbors(Tile tile)
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    Tile tmp = AtPosition(i, j);
                    if (tmp != null && tmp != tile)
                    {
                        yield return tmp;
                    }
                }
            }
        }

        public IEnumerable<Tile> GetValidNeighbors(Tile tile)
        {
            lock (loc)
            {
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        Tile tmp = AtPosition(tile.WorldCoor.X + i, tile.WorldCoor.Y + j);
                        if (tmp != null && tmp != tile)
                        {
                            if (i == j)
                            {
                                Tile tmp1 = AtPosition(tile.WorldCoor.X, tmp.WorldCoor.Y);
                                Tile tmp2 = AtPosition(tmp.WorldCoor.X, tile.WorldCoor.Y);
                                if (tmp1 != null && tmp2 != null)
                                {
                                    if (tmp1.IsWalkable && tmp2.IsWalkable)
                                        yield return tmp;
                                }
                            }
                            else if (tmp.IsWalkable)
                            {
                                yield return tmp;
                            }
                        }
                    }
                }
            }
        }
    }
}
