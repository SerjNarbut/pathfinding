﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace Pathfinding.World.Objects
{
    public interface IUnit : IMoveable, ISelectable
    {
        Vector2 Position { get; set; }
        Vector2 NextPosition { get; set; }
        Vector2 PrevPosition { get; set; }
    }
}
