﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Pathfinding.World.Objects
{
    public interface ISelectable
    {
        bool Selecteable { get; set; }
        bool Selected { get; set; }
        bool Preselected { get; set; }
        void Deselect();
        Rectangle GetPosition();
    }
}
