﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Pathfinding.World.Objects
{
    public interface IMoveable
    {
        bool Moved { get; }
        void Move(Vector2 position, GameTime gameTime);
    }
}
