using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Pathfinding.UI;
using Pathfinding.Util;
using System.Threading;
using Pathfinding.Algorithms;


namespace Pathfinding.World.Objects
{
    public class Unit : Microsoft.Xna.Framework.DrawableGameComponent, IUnit
    {
        public static readonly int SIZE = 32;
        public static readonly float SPEED = .1f;
        private SpriteBatch spriteBatch;
        private CameraComponent camera;
        private Thread findingThread;
        private AStar astar;
        private List<Vector2> path;

        public Unit(Game game, Vector2 position, CameraComponent camera) : base(game)
        {
            Position = PrevPosition = NextPosition = position;
            this.camera = camera;
            HasGoal = false;
            Goal = Vector2.Zero;
            astar = new AStar();
            astar.PathFind += new EventHandler<AStarEventsArgs>(astar_PathFind);
        }

        void astar_PathFind(object sender, AStarEventsArgs e)
        {
            path = e.Path;
            findingThread.Abort();
        }

        public override void Initialize()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            //Move(Goal,gameTime);
            if (HasGoal)
            {
                if (path == null && !astar.Started)
                {
                    Vector2 coord = UnitHelper.GetWorldPosition(this, camera);
                    astar.StartCell = AStar.Map.AtCoordinates(Position.X, Position.Y);
                    astar.EndCell = AStar.Map.AtCoordinates(Goal.X, Goal.Y);
                    astar.Start();
                    findingThread = new Thread(astar.Find);
                    findingThread.Start();
                }
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Vector2 pos = Vector2.Transform(Position, camera.InverseTransform);
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.Transform);
            spriteBatch.Draw(TextureLibrary.GetTexture("pixel"), new Rectangle((int)pos.X,(int)pos.Y,SIZE,SIZE), getColor());
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public Vector2 Position
        {
            get;
            set;
        }

        public Vector2 NextPosition
        {
            get;
            set;
        }

        public Vector2 PrevPosition
        {
            get;
            set;
        }

        public Vector2 Goal { get; set; }
        public bool HasGoal { get; set; }

        public bool Moved
        {
            get { return PrevPosition != Position; }
        }

        public void Move(Vector2 position, GameTime gameTime)
        {
            if (HasGoal)
            {
                Vector2 direction = Goal - Position;
                direction.Normalize();
                NextPosition += direction*(float)gameTime.ElapsedGameTime.TotalMilliseconds*SPEED;
                if (UnitHelper.NearPosition(NextPosition,Goal,0.7f))
                {
                    Goal = Vector2.Zero;
                    HasGoal = false;
                }
                PrevPosition = Position;
                Position = NextPosition;
            }
        }

        public bool Selected
        {
            get;
            set;
        }

        public bool Preselected
        {
            get;
            set;
        }

        public void Deselect()
        {
            Selected = false;
            Preselected = false;
        }

        public Rectangle GetPosition()
        {
            Vector2 pos = Vector2.Transform(Position, camera.InverseTransform);
            return new Rectangle((int)pos.X, (int)pos.Y, SIZE, SIZE);
        }

        public bool Selecteable
        {
            get;
            set;
        }

        private Color getColor()
        {
            Color col = Color.Blue;
            if (Preselected)
                col = Color.Green;
            else if (Selected)
                col = Color.Red;
            return col;
        }
    }
}
