﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Microsoft.Xna.Framework;
using Pathfinding.World.Map;
using Pathfinding.Util;

namespace Pathfinding.Algorithms
{
    class AStar
    {
        public event EventHandler<AStarEventsArgs> PathFind;
        public static TileMap Map { get; set; }
        public Tile StartCell { get; set; }
        public Tile EndCell { get; set; }
        public bool Started { get; set; }

        private BinaryHeap openList;
        private List<Tile> closedList;
        private List<Vector2> path;
        double[,] G;

        private Tile current;

        public void Start()
        {
            if (StartCell == null || EndCell == null)
            {
                return;
            }
            else
            {
                openList = new BinaryHeap();
                closedList = new List<Tile>();
                path = new List<Vector2>();
                current = StartCell;
                closedList.Add(current);
                Started = true;
                G = new double [ Map.SIZE, Map.SIZE ];
                G[current.WorldCoor.X, current.WorldCoor.Y] = 0;
            }
        }

        public void Find()
        {
            if (Started)
            {
                do
                {
                    foreach (Tile tile in Map.GetValidNeighbors(current))
                    {
                        if (!closedList.Contains(tile))
                        {
                            double costH = getCost(tile, EndCell) *10;
                            G[tile.WorldCoor.X, tile.WorldCoor.Y] = G[current.WorldCoor.X, current.WorldCoor.Y]+ 10;
                            Node n = new Node(tile, costH + G[tile.WorldCoor.X, tile.WorldCoor.Y] + 10);
                            openList.Add(n);
                        }
                    }

                    Node work = openList.PopMin();
                    current = work.Tile;
                    closedList.Add(current);
                    path.Add(new Vector2(current.ScreenCoord.X, current.ScreenCoord.Y));
                    if (closedList.Contains(EndCell))
                    {
                        if (PathFind != null)
                        {
                            PathFind(this, new AStarEventsArgs(path));
                            Started = false;
                        }
                    }
                } while (openList.HeapCount > 0);
                Started = false;
            }
   
        }

        private double getCost(Tile source, Tile destination)
        {
            return Math.Abs(destination.ScreenCoord.X - source.ScreenCoord.X) + Math.Abs(destination.ScreenCoord.Y - source.ScreenCoord.Y);
        }
    }

    class AStarEventsArgs : EventArgs
    {
        public List<Vector2> Path { get; set; }
        public AStarEventsArgs(List<Vector2> path)
        {
            Path = path;
        }
    }
}
