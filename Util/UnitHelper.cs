﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pathfinding.World.Objects;
using Microsoft.Xna.Framework;
using Pathfinding.UI;

namespace Pathfinding.Util
{
    static class UnitHelper
    {
        public static IEnumerable<IUnit> FilterSelectableUnits(IEnumerable source)
        {
            var resut = new LinkedList<IUnit>();
            foreach(var unit in source.OfType<IUnit>().Where( i => i.Selecteable))
            {
                resut.AddLast(unit);
            }
            return resut;
        }

        public static IEnumerable<IUnit> FilterSelectedUnit(IEnumerable<IUnit> source)
        {
            var resut = new LinkedList<IUnit>();
            foreach(var unit in source.Where(i => i.Selected))
            {
                resut.AddLast(unit);
            }
            return resut;
        }

        public static Vector2 GetWorldPosition(IUnit unit, CameraComponent camera)
        {
            return Vector2.Transform(unit.Position, camera.InverseTransform);
        }

        public static Vector2 GetScreenPosition(IUnit unit, CameraComponent camera)
        {
            return Vector2.Transform(unit.Position, camera.Transform);
        }

        public static bool NearPosition(Vector2 position, Vector2 goal, float prec)
        {
            return Math.Abs(Vector2.Distance(position, goal)) <= prec;
        }
    }
}
