﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework.Graphics;

namespace Pathfinding.Util
{
    static class TextureLibrary
    {
        private static Dictionary<string, Texture2D> textureBase = null;

        static TextureLibrary()
        {
            if (textureBase == null)
            {
                textureBase = new Dictionary<string, Texture2D>();
            }
        }

        public static  void AddTexture(string name, Texture2D texture)
        {
            if(!textureBase.ContainsKey(name))
            {
                textureBase.Add(name, texture);
            }
        }

        public static Texture2D GetTexture(string name)
        {
            if (textureBase.ContainsKey(name))
            {
                return textureBase[name];
            }
            else
            {
                throw new ArgumentOutOfRangeException(string.Format("texture {0} not found",name));
            }
        }

        public static void Clear()
        {
            textureBase.Clear();
        }
    }
}
