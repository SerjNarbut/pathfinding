﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pathfinding.World.Map;

namespace Pathfinding.Util
{
    class BinaryHeap
    {
        private List<Node> items;

        public BinaryHeap()
        {
            items = new List<Node>();
        }

        public int HeapCount
        {
            get
            {
                return items.Count;
            }
        }

        public void Add(Node node)
        {
            items.Add(node);

            int index = HeapCount - 1;
            int parentIndex = (index - 1) / 2;

            while (index > 0 && items[parentIndex].F > items[index].F)
            {
                swap(index, parentIndex);
                index = parentIndex;
                parentIndex = (index - 1) / 2;
            }
        }

        public Node PopMin()
        {
            var val = items[0];
            items[0] = items[HeapCount - 1];
            items.RemoveAt(HeapCount - 1);
            balance(0);
            return val;
        }

        public Node PeakMin()
        {
            return items[0];
        }

        private void swap(int indexOne, int indexTwo)
        {
            var temp = items[indexOne];
            items[indexOne] = items[indexTwo];
            items[indexTwo] = temp;
        }

        private void balance(int index)
        {
            int left, right, current;
            while (true)
            {
                left = 2 * index + 1;
                right = left + 1;
                current = index;

                if (left < HeapCount && items[left].F < items[current].F)
                {
                    current = left;
                }

                if (right < HeapCount && items[right].F < items[current].F)
                {
                    current = right;
                }

                if (current == index)
                    break;

                swap(index, current);
                index = current;
            }
        }
    }

    struct Node
    {
        public Node(Tile t, double f)
        {
            Tile = t;
            F = f;
        }
        public Tile Tile;
        public double F;
    }
}
