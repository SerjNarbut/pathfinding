using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Pathfinding.UI
{

    public class CameraComponent : Microsoft.Xna.Framework.GameComponent
    {

        protected float zoom;
        protected int lastScroll;
        protected Vector2 position;
        protected Viewport viewport;
        protected Matrix transform;
        protected Matrix inverse;
        protected MouseState mstate;
        protected KeyboardState kstate;

        public CameraComponent(Game game) : base(game)
        {
            zoom = 1f;
            position = Vector2.Zero;
            lastScroll = 0;
        }

        public float Zoom
        {
            get { return zoom; }
            set { zoom = value; }
        }

        public Matrix Transform
        {
            get { return transform; }
        }

        public Matrix InverseTransform { get { return inverse; } }

        public Rectangle ViewportRectangle
        {
            get
            {
                return new Rectangle(viewport.X,viewport.Y,viewport.Width,viewport.Height);
            }
        }

        public override void Initialize()
        {
            viewport = Game.GraphicsDevice.Viewport;
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            inputUpdate();
            transform = Matrix.CreateTranslation(new Vector3(-position, 0f))*Matrix.CreateScale(Zoom);
            inverse = Matrix.Invert(transform);
            base.Update(gameTime);
        }

        protected void inputUpdate()
        {
            mstate = Mouse.GetState();
            kstate = Keyboard.GetState();

            if (mstate.ScrollWheelValue > lastScroll)
            {
                zoom += 0.02f;
                lastScroll = mstate.ScrollWheelValue;
            }
            else if (mstate.ScrollWheelValue < lastScroll)
            {
                zoom -= 0.02f;
                lastScroll = mstate.ScrollWheelValue;
            }

            zoom = MathHelper.Clamp(zoom, 0.5f, 2f);

            Vector2 mov = Vector2.Zero;
            if(kstate.IsKeyDown(Keys.Right))
            {
                mov.X += 5f / zoom;
            }
            if (kstate.IsKeyDown(Keys.Left))
            {
                mov.X -= 5f / zoom;
            }

            if (kstate.IsKeyDown(Keys.Down))
            {
                mov.Y -= 5f / zoom;
            }
            if (kstate.IsKeyDown(Keys.Up))
            {
                mov.Y += 5f / zoom;
            }

            position += mov;

        }
    }
}
